'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;


let OrderSchema = new Schema({
  Customer_Email: String,
  count: Number,
  template: Buffer,
  form: String,
  type: String,
  size: String,
  price: Number,
  comment: String,
  created_on: {
    type: Date,
    default: Date.now()
  }
});

module.exports = mongoose.model('Order', OrderSchema);
