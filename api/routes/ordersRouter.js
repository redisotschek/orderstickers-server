var express = require('express');
var router = express.Router();
var prices = require('../controllers/ordersController');


/* GET home page. */
router.get('/', function(req, res, next) {
  prices.getPrices(req, res);
});

router.post('/newOrder', function(req, res, next) {
  prices.createOrder(req, res);
});

module.exports = router;
