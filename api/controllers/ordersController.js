var mongoose = require('mongoose'),
  Order = mongoose.model('Order');

var prices = {
  gay: 'aaaaaaaaa',
  meme: 'jojo',
  sad: 'xxx is dead'
};

exports.getPrices = function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(prices));
};

exports.createOrder = function (req, res) {
  var newOrder = new Order(req.body);
  newOrder.save(function(err, order) {
    if (err)
      res.send(err);
    res.send(JSON.stringify(order));
  });
};
