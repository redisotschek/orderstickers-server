var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var multiparty = require('multiparty');
const fs = require('fs');
var mongoose = require('mongoose');

// mongoose.connect('mongodb://localhost/stickers_db', { useNewUrlParser: true }).then(
//   function(response) {
//     console.log('Successfully connected');
//   },
//   function(error) {
//     console.log(error);
//   }
// );

var orderModel = require("./api/models/orderModel");

var indexRouter = require('./api/routes/index');
var usersRouter = require('./api/routes/users');
var pricesRouter = require('./api/routes/ordersRouter');

var app = require('express')();

var mailer = require('nodemailer');

var whitelist = ['http://orderstickers-dev.ga', 'http://orderstickers.ru', 'http://localhost:8080'];

var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  },
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.use(cors(corsOptions));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use('/', indexRouter);
// app.use('/users', usersRouter);
// app.use('/prices', pricesRouter);
var transporter = mailer.createTransport({
  host: "smtp.mail.ru",
  port: 465,
  use_authentication: true,
  auth: {
    user: 'orderstickers@mail.ru',
    pass: 'nikita0660'
  }
});

function sendMail (options) {
  return new Promise((resolve, reject) => {
    transporter.sendMail(options, (error, info) => {
      if(error){
        reject(error);
      } else {
        resolve(info);
      }
    });

    transporter.close();
  });
}

app.post('/newOrder', function (req, res, next) {

  var form = new multiparty.Form();
  form.parse(req, function(err, fields, files) {
    console.log(files.file[0].headers['content-type']);

    var ownerMailOptions = {
      from: 'orderstickers@mail.ru',
      to: 'orderstickers@mail.ru',
      subject: 'Новый заказ!',
      text: 'Ваш новый заказ: \n' +
        `Почта пользователя: ${fields.customer_email} \n Материал: ${fields.material} \n Количество: ${fields.count} \n Форма: ${fields.form} \n Ламинация: ${fields.lamination} \n Размер: ${fields.size} \n Цена: ${fields.price} \n Комментарий: ${fields.comment} \n Промокод: ${fields.promocode}`,
      attachments: [{filename: files.file[0].originalFilename, content: files.file[0], contentType: files.file[0].headers['content-type'], cid: 'cidprefix_' + files.file[0].originalFilename}]
    }

    var clientMailOptions = {
      from: 'orderstickers@mail.ru',
      to: fields.customer_email,
      subject: 'Заказ на сайте orderstickers.ru',
      text: 'Ваш заказ принят в обработку! Спасибо, что вы с нами.'
    }

    sendMail(ownerMailOptions)
      .then(info => res.send(info))
      .catch(err => res.send(err))

    sendMail(clientMailOptions)
  });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
